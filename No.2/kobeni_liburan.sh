#!/bin/bash

# * */10 * * * bash kobeni_liburan.sh 1

task1(){
  i=1
  # Cek apakah folder sudah ada atau belum
  while [ -d kumpulan_$i ]
  do
    i=$((i+1))
  done

  # Buat folder baru
  mkdir kumpulan_$i

  # Cek jam sekarang
  TIME=$(date +"%H")
  # Convert ke integer
  TIME=$(expr $TIME + 0)

  # Masuk ke folder baru
  cd kumpulan_$i

  # Jika pada jam 00:00 download 1 gambar aja
  if [[ $TIME -eq 0 && $(date +"%M") -eq 0 ]]
  then
    TIME=1
  fi

  # Looping sesuai waktu
  for ((j=1; j<=$TIME; j++))
  do
    wget -O perjalanan_$j.jpg https://loremflickr.com/320/240/indonesia
  done
}

# Problem 2b
task2(){
  loop=1
  while [ -d kumpulan_$loop ]
  do 
    # apabila sudah dikompres, maka skip
    if [ -f devil_$loop.zip ]
    then
      loop=$((loop+1))
      continue
    fi

    # Kompress folder
    zip -r  devil_$loop.zip kumpulan_$loop
    loop=$((loop+1))
  done
}

if [ ! $1]
  then 
  echo "Error: You must enter number 1 or 2"
  exit
fi

if [ $1 -eq 1 ]
then
  task1
elif [ $1 -eq 2 ]
then
  task2
fi

