#!/bin/bash

#buat file deskripsi dengan nama file bebas
filename=$(date "+%H":"%M %d":"%m":"%y backup")
#semua backup nanti akan disimpan di file name
#%H adalah jam, %M adalah menit, %d adalah tanggal, %m adalah bulan,%y adalah tahun


#simpan jam dengan variabel hour
hour=$(date "+%H")

#buat suatu variabel array untuk menyimpan value
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

#Penggunaan sistem cipher untuk menggeser barisan alfabet
#cipher 1 -> untuk huruf kecil
cipher1="${lowercase[$hour]}-z}a-${lowercase[$hour-1]}"

#chiper 2 -> untuk huruf besar
cipher2="${uppercase[$hour]}-Z}A-${uppercase[$hour-1]}"

#Setelah selesai pindah ke directory yang digunakan untuk menyimpan backup
cd '/home/kevin/Documents/PRAKTIKUM1/No4/'

#Tulis hasil backup ke filename.txt
echo -n "$(cat 18:45\ 03:03:23.txt tR $cipher1 'a-z' | tr $cipher2 'A_Z')" > "$filename.txt"

