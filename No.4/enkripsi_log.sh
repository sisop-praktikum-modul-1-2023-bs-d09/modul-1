#!/bin/bash

#buat file enkripsi
filename=$(date "+%H":"%M %d":"%m":"%y")

#cari variabel untuk mendapatkan jam sekarang
hour=$(date "+%H")

#buat arry untuk dapat value setiap huruf
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

#buat cipher
#cipher untuk huruf kecil
cipher1="${lowercase[$hour]}-z}a-${lowercase[$hour-1]}"

#cipher untuk huruf besar
cipher2="${uppercase[$hour]}-Z}A-${uppercase[$hour-1]}"

#directory penyimpanan
cd '/home/kevin/Documents/PRAKTIKUM1/No4/'

#menyimpan string hasil enkripsi
echo -n "$(cat /var/log/syslog | tr 'a-z' $cipher1 | tr 'A-Z' $cipher2)" > "$filename.txt"

#backup setiap 2 jam sekali
0 */2 * * * /bin/bash /home/kevin/Documents/PRAKTIKUM1/No4/enkripsi_log.sh

