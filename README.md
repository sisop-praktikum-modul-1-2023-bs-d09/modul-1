#  Sistem Operasi Modul 1
### Kelompok D09
| Nama | NRP |
| ------ | ------ |
| Christian Kevin Emor | 5025211153 |
| Andrian Tambunan | 5025211018 |
| Helmi Abiyu Mahendra | 5025211061 |

### Daftar isi
- [Problem 1]
- [Deskripsi Problem 1]
    - [Point 1A]
        - [Deskripsi Problem Point 1A]
        - [Solusi Point 1A]
        - [Code Point 1A]
        - [Penjelasan Point 1A]
        - [Output Point 1A]
    - [Point 1B]
        - [Deskripsi Problem Point 1B]
        - [Solusi Point 1B]
        - [Code Point 1B]
        - [Penjelasan Point 1B]
        - [Output Point 1B]
    - [Point 1C]
        - [Deskripsi Problem Point 1C]
        - [Solusi Point 1C]
        - [Code Point 1C]
        - [Penjelasan Point 1C]
        - [Output Point 1C]
    - [Point 1D]
        - [Deskripsi Problem Point 1D]
        - [Solusi Point 1D]
        - [Code Point 1D]
        - [Penjelasan Point 1D]
        - [Output Point 1D]
- [Problem 2]
- [Deskripsi Problem]
   - [Point 2A]
        - [Deskripsi Problem 2A]
   - [Point 2B]
        - [Deskripsi Problem 2B]
        - [Solusi 2B]
        - [Penjelasan 2B]

- [Problem 3]
- [Deskripsi Problem 3]
    -  [Registration (louis.sh)]
    -  [Login (retep.sh)]
    -  [Point 3A]
        - [Deskripsi Problem 3A]
        - [Merged 3A]
        - [Eksekusi User Registration 3A]
        - [Output 3A]
    -  [Point 3B]
        - [Deskripsi Problem 3B]
        - [Registration n Login mechanism 3B]
        - [Registration Failed 3B]
        - [Registration Succesful 3B]
        - [Login Failed 3B]
        - [Login Succesful 3B]
        - [Registration succesful message 3B]
        - [Registration failed message 3B]
        - [Login succesful message 3B]
        - [Login failed message 3B]
- [Problem 4]
- [Penjelasan Soal]
    -  [Point A]
        - [Solusi 4A]
        - [Code 4A]
        - [Penjelasan 4A]
    -  [Point B]
        - [Solusi 4B]
        - [Penjelasan 4B]
    -  [Point C]
        - [Solusi 4C]
        - [Penjelasan 4C]
    -  [Point D]
        - [Solusi 4D]
        - [Penjelasan 4D]

### Problem 1
##### Deskripsi Problem:
Bochi Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi :

#### Point 1A
##### Deskripsi Problem:
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
##### Solusi
Untuk menyelesaikan Problem 1A, Bocchi perlu menemukan universitas yang berada di Jepang, yang mana menggunakan kode `JP` pada kolom `Location Code` dan mengurutkannya berdasarkan kolom `Rank`. Kemudian, Bocchi dapat memilih 5 data teratas.
##### Code
```sh
sort -t, -k1n '2023 QS World University Rankings.csv' | awk -F',' '$3 == "JP" {print $2}' | head -n 5 
```
##### Penjelasan
1. `sort -t, -k1n`: Untuk mengurutkan data berdasarkan kolom ke-1 secara ascending.
2. `'2023 QS World University Rankings.csv'`: Untuk menentukan file yang akan difilter.
3. `awk -F','`: Untuk menentukan pembatas yang digunakan, disini menggunakan koma (,) sebagai pemisah.
4. `$3 == "JP"`: Untuk memfilter data pada kolom ke 3, yaitu JP.
5. `{print $2}`: Untuk menampilkan kolom yang ingin di tampilkan, yaitu kolom ke-2.
6. `head -n 5`: Untuk memilih 5 hasil teratas.
##### Output
```sh
The University of Tokyo
Kyoto University
Tokyo Institute of Technology (Tokyo Tech)
Osaka University
Tohoku University
```

#### Point 1B
##### Deskripsi Problem:
Karena Bocchi kurang percaya diri, coba cari Faculty Student Score (fsr score) yang paling rendah diantara 5 Universitas di point 1A.
##### Solusi
Untuk menyelesaikan Problem 1B, Bochi hanya perlu mengurutkan `Fsr score` dari hasil filter point 1A
##### Code
```sh
sort -t, -k1n '2023 QS World University Rankings.csv' | awk -F',' '$3 == "JP"' | head -n 5\
 | sort -t, -k9n | awk -F',' '{print $2}' | head -n 1
 ```
 ##### Penjelasan
1. `sort -t, -k1n`: Untuk mengurutkan data berdasarkan kolom ke-1 secara ascending.
2. `'2023 QS World University Rankings.csv'`: Untuk menentukan file yang akan difilter.
3. `awk -F','`: Untuk menentukan pembatas yang digunakan, disini menggunakan koma (,) sebagai pemisah.
4. `$3 == "JP"`: Untuk memfilter data pada kolom ke 3, yaitu JP.
5. `head -n 5`: Untuk memilih 5 hasil teratas.
6. `sort -t, -k9n`: Untuk mengurutkan data berdasarkan kolom ke-9 secara ascending.
7. `{print $2}`: Untuk menampilkan kolom yang ingin di tampilkan, yaitu kolom ke-2.
8. `head -n 1`: Untuk memilih 1 hasil teratas.
##### Output
```sh
Osaka University
```

#### Point 1C
##### Deskripsi Problem:
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank (Ger rank) paling tinggi.
##### Solusi
Untuk menyelesaikan Problem 1C, Bocchi perlu menemukan universitas yang berada di Jepang, yang mana menggunakan kode `JP` pada kolom `Location Code` dan mengurutkannya berdasarkan kolom ke-20 yaitu `Ger Rank`. Kemudian, Bocchi dapat memilih 10 data teratas.
##### Code
```sh
sort -t, -k20n '2023 QS World University Rankings.csv' | awk -F',' '$3 == "JP" {print $2}' | head -n 10
 ```
 ##### Penjelasan
1. `sort -t, -k20n`: Untuk mengurutkan data berdasarkan kolom ke-20 secara ascending.
2. `'2023 QS World University Rankings.csv'`: Untuk menentukan file yang akan difilter.
3. `awk -F','`: Untuk menentukan pembatas yang digunakan, disini menggunakan koma (,) sebagai pemisah.
4. `$3 == "JP"`: Untuk memfilter data pada kolom ke 3, yaitu JP.
5. `{print $2}`: Untuk menampilkan kolom yang ingin di tampilkan, yaitu kolom ke-2.
6. `head -n 10`: Untuk memilih 1 hasil teratas.
##### Output
```sh
The University of Tokyo
Keio University
Waseda University
Hitotsubashi University
Tokyo University of Science
Kyoto University
Nagoya University
Tokyo Institute of Technology (Tokyo Tech)
International Christian University
Kyushu University
```

#### Point 1D
##### Deskripsi Problem:
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci "keren".
##### Solusi
Untuk menyelesaikan Problem 1D, Bocchi hanya perlu memfilter file dengan kata kunci `keren`.
##### Code
```sh
awk -F',' '/Keren/ {print $2}' '2023 QS World University Rankings.csv'
 ```
 ##### Penjelasan
1. `awk -F','`: Untuk menentukan pembatas yang digunakan, disini menggunakan koma (,) sebagai pemisah.
2. `/Keren/`: Untuk menandakan bahwa kata kunci filter adalah "Keren"
3. `{print $2}`: Untuk menampilkan kolom yang ingin di tampilkan, yaitu kolom ke-2.
4. `'2023 QS World University Rankings.csv'`: Untuk menentukan file yang akan difilter.
##### Output
```sh
Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren)
```

[Problem 1 ]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#problem-1>
[Deskripsi Problem 1]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem>


[Point 1A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-1a>
[Deskripsi Problem Point 1A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem-1>
[Solusi Point 1A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#solusi>
[Code Point 1A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#code>
[Penjelasan Point 1A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan>
[Output Point 1A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#output>

[Point 1B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-1b>
[Deskripsi Problem Point 1B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem-2>
[Solusi Point 1B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#solusi-1>
[Code Point 1B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#code-1>
[Penjelasan Point 1B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan-1>
[Output Point 1B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#output-1>


[Point 1C]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-1c>
[Deskripsi Problem Point 1C]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem-3>
[Solusi Point 1C]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#solusi-2>
[Code Point 1C]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#code-2>
[Penjelasan Point 1C]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan-2>
[Output Point 1C]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#output-2>

[Point 1D]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-1d>
[Deskripsi Problem Point 1D]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem-4>
[Solusi Point 1D]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#solusi-3>
[Code Point 1D]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#code-3>
[Penjelasan Point 1D]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan-3>
[Output Point 1D]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#output-3>

#### Problem 2
##### Deskripsi Problem
Script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang
(Apabila pukul 00:00 cukup download 1 gambar saja). 
Gambarnya didownload setiap 10 jam sekali
mulai dari saat script dijalankan.

```sh
i=1
  # Cek apakah folder sudah ada atau belum
  while [ -d kumpulan_$i ]
  do
    i=$((i+1))
  done

  # Buat folder baru
  mkdir kumpulan_$i

  # Cek jam sekarang
  TIME=$(date +"%H")
  # Convert ke integer
  TIME=$(expr $TIME + 0)

  # Masuk ke folder baru
  cd kumpulan_$i

  # Jika pada jam 00:00 download 1 gambar aja
  if [[ $TIME -eq 0 && $(date +"%M") -eq 0 ]]
  then
    TIME=1
  fi

  # Looping sesuai waktu
  for ((j=1; j<=$TIME; j++))
  do
    wget -O perjalanan_$j.jpg https://loremflickr.com/320/240/indonesia
  done
```

Adapun ketentuan file dan folder yang
akan dibuat adalah sebagai berikut:

##### Point 2A
###### Deskripsi Problem 2A

○ File yang didownload memilki format nama
“perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan
file yang download (perjalanan_1, perjalanan_2, dst)
```sh
#wget = get(download) img dari web
wget -O perjalanan_$j.jpg https://loremflickr.com/320/240/indonesia
```
○ File batch yang didownload akan dimasukkan ke dalam folder
dengan format nama “kumpulan_NOMOR.FOLDER” dengan
NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1,
kumpulan_2, dst)
```sh
# Masuk ke folder baru
  cd kumpulan_$i
```

##### Point B
###### Deskripsi
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

###### Solusi
Deklarasikan variabel loop seperti berikut
``` while [ -d kumpulan_$loop ]
  do
    if [ -f devil_$loop.zip ]
    then
      loop=$((loop+1))
      continue
    fi

    zip -r  devil_$loop.zip kumpulan_$loop
    loop=$((loop+1))
  done

```
###### Penjelasan
1. `while [ -d kumpulan_$loop ]` Melakukan perulangan sampai menemukan folder dengan nama kumpulan_$loop
2. `if [ -f devil_$loop.zip ]` Jika folder tersebut sudah pernah di-zip, lanjutkan ke folder berikutnya
3. `zip -r  devil_$loop.zip kumpulan_$loop` Melakukan zip kepada folder yang dipilih secara rekursif

[Problem 2]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#problem-2>
[Deskripsi Problem]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem-5>
[Point 2A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-2a>
[Deskripsi Problem 2A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem-2a>

[Point 2B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-b>
[Deskripsi Problem 2B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi>
[Solusi 2B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#solusi-4>
[Penjelasan 2B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan-4>


#### Problem 3
##### Deskripsi Problem
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari
setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin
juga membuat sistem login yang dibuat di script retep.sh

###### Registration (louis.sh)

```sh
#penerimaan input username
echo "INPUT USERNAME :"
read username
#input disimpan dalam file user.txt pada folder users
existFolder=$(ls | grep -w "users")
if [ -z "$existFolder" ]
then
        mkdir users > user.txt
        mv user.txt ./users
fi
#pengambilan taken username dari data username pada user.txt
takenUsername=$(grep -w "$username" ./users/user.txt)
```

###### Login (retep.sh)

```sh
read username

takenUsername=$(grep -w "$username" ./users/user.txt)
#menerima input hingga menemukan username yang
#terdaftar pada list takenUsername
while [ -z  "$takenUsername" ]
do
        echo "Username not identified, please try again"
        echo -n "Username: "
        read username
        takenUsername=$(grep -w "$username" ./users/user.txt)
done

```



### Point 3A
##### Deskripsi Problem:
Untuk memastikan password pada register dan login aman, maka ketika
proses input passwordnya harus memiliki ketentuan berikut
###### Constraints: 
-Minimal 8 karakter
```sh
${#password} -ge 8
```
-Memiliki minimal 1 huruf kapital dan 1 huruf kecil
```sh
$password =~ [A-Z] && $password =~ [a-z] && $password =~ [0-9]
```
-Alphanumeric
```sh
$password =~ [^[:alnum:]]
```
-Tidak boleh sama dengan username
```sh
"$lowerPassword" != *"$lowerUsername"*
#karakter asterisk (*) di sini sebagai penanda "contain"
#artinya, password terdeteksi mengandung username, bukan sama persis
#dengan username
```
-Tidak boleh menggunakan kata chicken atau ernie
```sh
"$lowerPassword" != "*"chicken"* && "$lowerPassword"  != *"ernie"*
```

###### Merged
```sh
#mengubah menjadi lowercase agar komparasi lebih akurat
lowerPassword=$(echo $password | tr '[:upper:]' '[:lower:]')
lowerUsername=$(echo $username | tr '[:upper:]' '[:lower:]')

#pengulangan terus dilakukan sampai password tidak menyalahi ketentuan
until [[ $password =~ [A-Z] && $password =~ [a-z] && $password =~ [0-9] && $password =~ [^[:alnum:]] && "$lowerPassword" != "*"chicken"* && "$lowerPassword"  != *"ernie"* && ${#password} -ge 8 ]]

do
        echo -e "\nPassword must be at least 8 characters that contain uppercase, lowercase, alphanumeric, not the same word as username, can't contain words 'chicken', and 'ernie'\n"

        echo -n "Password: "
        read -s password
        lowerPassword=$(echo $password | tr '[:upper:]' '[:lower:]')
done
```

###### Eksekusi User Registration
```sh
USER REGISTRATION
INPUT USERNAME :
Pekora
Password: 
Password must be at least 8 characters that contain uppercase, lowercase, alphanumeric, not the same word as username, can't contain
words 'chicken', and 'ernie'
Password: 

REGISTRATION SUCCESFUL
```

###### Output
Data user "Pekora" yang tercatat pada user.txt
```sh
Pekora : PekoPeko44;
#sudah sesuai dengan ketentuan password yang baik
```


#### Point 3B
##### Deskripsi Problem:
 Setiap percobaan login dan register akan tercatat pada log.txt dengan
format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan
berbeda tergantung aksi yang dilakukan user.
##### Registration n Login mechanism

###### Registration Failed
```sh
takenUsername=$(grep -w "$username" ./users/user.txt)

until [ -z "$takenUsername" ]
#sampai username baru tidak sama dengan yang telah terdaftar
do
        echo "Username already taken, please try again"
        echo "$(date +"%D %T") REGISTER: ERROR User already exists" >> log.txt
        echo -n "INPUT USERNAME: "
        read username
        takenUsername=$(grep -w "$username" ./users/user.txt)
done
```

###### Registration Succesful
```sh
echo "$(date +"%D %T") REGISTER: INFO User $username registered successfully" >> log.txt
```



###### Login Failed
```sh
read -s password

##ketika password yang diinputkan tidak sesuai dengan username yang bersangkutan
until [ $password == $existPassword ]
do
        echo "Password incorrect, try again"
        echo "$(date +"%D %T") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
        echo -n "Password: "
        read -s password
done
##variabel date D untuk tanggal, dan T untuk waktu
```

###### Login Succesful
```sh
echo "$(date +"%D %T") LOGIN: INFO User $username logged in" >> log.txt
```


###### Registration succesful message
REGISTER: INFO User USERNAME registered successfully

```sh
USER REGISTRATION
INPUT USERNAME :
Backnumber
Password: 

REGISTRATION SUCCESFUL
```
pada log.txt

```sh
03/10/23 17:01:36 REGISTER: INFO User Backnumber registered successfully
```
###### Registration failed message    
REGISTER: ERROR User already exists 
(registrasi dengan username yg sudah ada)
```sh
USER REGISTRATION
INPUT USERNAME :
Pekora
Username already taken, please try again
INPUT USERNAME: 
```
pada log.txt
```sh
03/10/23 16:54:35 REGISTER: ERROR User already exists
```

###### Login succesful message
LOGIN: INFO User USERNAME logged in
```sh
LOGIN
Username: Kyokotsu
Password: 

LOGIN SUCCESFUL
```
pada log.txt
```sh
03/10/23 17:26:17 LOGIN: INFO User Kyokotsu logged in
```

###### Login failed message
LOGIN: ERROR Failed login attempt on user USERNAME 
(Login gagal karena password salah)

```sh
LOGIN
Username: Backnumber
Password: Password incorrect, try again
```

pada log.txt
```sh
03/10/23 17:09:32 LOGIN: ERROR Failed login attempt on user Backnumber
```
[Problem 3]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#problem-3>
[Deskripsi Problem 3]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem-5>
[Registration (louis.sh)]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#registration-louissh>
[Login (retep.sh)]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#login-retepsh>
[Point 3A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-3a>
[Deskripsi Problem 3A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem-6>
[Merged 3A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#merged>
[Eksekusi User Registration 3A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#eksekusi-user-registration>
[Output 3A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#output-4>
[Point 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-3b>
[Deskripsi Problem 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#deskripsi-problem-7>
[Registration n Login mechanism 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#registration-n-login-mechanism>
[Registration Failed 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#registration-failed>
[Registration Succesful 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#registration-succesful>
[Login Failed 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#login-failed>
[Login Succesful 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#login-succesful>
[Registration succesful message 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#registration-succesful-message>
[Registration failed message 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#registration-failed-message>
[Login succesful message 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#login-succesful-message>
[Login failed message 3B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#login-failed-message>


### Problem 4
##### Penjelasan Soal
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia
mengharuskan dirinya untuk mencatat log system komputernya. File syslog
tersebut harus memiliki ketentuan :

##### Point A
Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
###### Solusi
Untuk menyelesaikan masalah tersebut, Johan perlu mengambil waktu saat ini, termasuk jam, menit, tanggal, bulan, dan tahun menggunakan perintah date. Setelah itu, ia dapat memasukkan elemen-elemen tersebut ke dalam nama file (filename) dengan urutan format yang diminta.

###### Code
```
filename=$(date "+%H":"%M %d":"%m":"%y")

echo -n "$(cat /var/log/syslog | tr 'a-z' $cipher1 | tr 'A-Z' $cipher2)" > "$filename.txt"
```

###### Penjelasan
1. `filename` deklarasi variabel dengan nama filename yang memiliki tipe data `date` dan berisikan `H` adalah Hour, `M` adalah minute, `d` adalah day, `m` adalah month, dan `y` adalah year
2. `echo -n "$(cat /var/log/syslog | tr 'a-z' $cipher1 | tr 'A-Z' $cipher2)" > "$filename.txt"` adalah kode untuk menyimpan file dengan nama `filename` yang telah kita deklarasikan di awal serta bertipe `txt`

##### Point B
Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:

○ Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
○ Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf keempat belas, dan seterusnya.
○ Setelah huruf z akan kembali ke huruf a
###### Solusi
Untuk menyelesaikan masalah tersebut, Johan perlu terlebih dahulu mengambil waktu saat ini (jam) menggunakan perintah date dan memanggil elemen %H. Kemudian ia harus menyimpan hasilnya ke dalam variabel hour yang akan digunakan untuk sistem enkripsi.

```
hour=$(date "+%H")
```

###### Penjelasan
1. `hour` akan mendapatkan jam saat ini
2. `+%H` merupakan format specifier untuk dapat 24 jam. Bedanya `H` biasa dengan `+%H` adalah saat `+H` biasa saat menerima input misalnya 20.25 maka akan ditampilkan 20.25. Sedangkan `+%H` pada saat 20.25 ia akan menampilkan hour nya saja yaitu 20

Langkah berikutnya adalah membuat dua array, masing-masing berisi huruf kecil dan huruf besar, untuk mendapatkan urutan alfabet. Array ini dapat dibuat sebagai berikut:

```lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
```

Selanjutnya kita harus membuat chiper untuk menggeser, disini saya mendeklarasikan chiper menjadi 2 yaitu chiper 1 untuk lowercase dan chiper 2 untuk uppercase

```
cipher1="${lowercase[$hour]}-z}a-${lowercase[$hour-1]}"
cipher2="${uppercase[$hour]}-Z}A-${uppercase[$hour-1]}"
```

###### Penjelasan
1. `${lowercase[$hour]}` dan `${uppercase[$hour]}` akan mengambil karakter pada array yang terdapat pada indeks ke hour
2. `${lowercase[$hour-1]}` dan `${uppercase[$hour-1]}` akan menggeser ke kiri agar dapat mengakses z, mengingat hour adalah 24 dan alfabet sebanyak 25 sehingga saat kita mengambil input hour maka alfabet 'z' tidak akan pernah terpanggil

##### Point C
Buat juga script untuk dekripsinya.
###### Solusi
untuk file deskripsi kodingan sama persis hanya berbeda saat menampilkan echo dari deskripsi. File deskripsi berguna untuk menerjermahkan kembali file enkripsi yang telah kita acak diawal tadi

```
echo -n "$(cat 18:45\ 03:03:23.txt tR $cipher1 'a-z' | tr $cipher2 'A_Z')" > "$filename.txt"
```

###### Penjelasan
1. `echo -n` digunakan untuk menampilkan baris teks tanpa adanya new line
2. `cat` digunakan untuk menampilkan isi dari file `18:45\ 03:03:23.txt` 
3. `tR $cipher1 'a-z'` untuk men translate (menerjermahkan) seluruh cipher 1 menjadi lowercase
4. `|` pipe digunakan untuk mengarahkan output menjadi input selanjutnya
5. `>` file akan disimpan dengan nama `"$filename.txt"`


##### Point D
Backup file syslog setiap 2 jam untuk dikumpulkan
###### Solusi
Bisa menggunakan cronjobs untuk membuat backup file tersebut

```
0 */2 * * * /bin/bash /home/kevin/Documents/PRAKTIKUM1/No4/enkripsi_log.sh
```

###### Penjelasan
1. `0` parameter pertama adalah menit. Karena kita butuh tepat 2 jam, maka dapat kita tulis 0.
2. `*/2` parameter kedua adalah jam, dimana penulisan seperti itu berarti bahwa perintah akan dilakukan tiap 2 jam sekali.
3. `* * *` parameter ketiga, empat, dan lima dapat kita isi * karena kita tidak butuh tanggal, bulan, atau hari tertentu untuk mengeksekusi perintah soal.
4. Parameter terakhir adalah command serta file path dari perintah yang akan dieksekusi yang dapat disesuaikan letak filenya.

[Problem 4]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#problem-4>
[Penjelasan Soal]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan-soal>
[Point A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-a>
[Solusi 4A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#solusi-4>
[Code 4A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#code-4>
[Penjelasan 4A]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan-4>
[Point B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-b>
[Solusi 4B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#solusi-5>

[Penjelasan 4B]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan-5>
[Point C]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-c>
[Solusi 4C]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#solusi-6>

[Penjelasan 4C]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan-7>
[Point D]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#point-d>
[Solusi 4D]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#solusi-7>

[Penjelasan 4D]: <https://gitlab.com/sisop-praktikum-modul-1-2023-bs-d09/modul-1/-/blob/main/README.md#penjelasan-8>