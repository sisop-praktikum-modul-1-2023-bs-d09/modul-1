#!/bin/bash

echo "USER REGISTRATION"

echo "INPUT USERNAME :"
read username

existFolder=$(ls | grep -w "users")
if [ -z "$existFolder" ]
then
	mkdir users > user.txt
	mv user.txt ./users
fi

takenUsername=$(grep -w "$username" ./users/user.txt)

until [ -z "$takenUsername" ]
do
	echo "Username already taken, please try again"
	echo "$(date +"%D %T") REGISTER: ERROR User already exists" >> log.txt
	echo -n "INPUT USERNAME: "
	read username
	takenUsername=$(grep -w "$username" ./users/user.txt)
done

echo -n "Password: "
read -s password

lowerPassword=$(echo $password | tr '[:upper:]' '[:lower:]')
lowerUsername=$(echo $username | tr '[:upper:]' '[:lower:]')


#alnum alphanumeric
until [[ $password =~ [A-Z] && $password =~ [a-z] && $password =~ [0-9] && $password =~ [^[:alnum:]] && "$lowerPassword" != *"$lowerUsername"* && "$lowerPassword" != *"chicken"* && "$lowerPassword" != *"ernie"* && ${#password} -ge 8 ]]
do
	echo -e "\nPassword must be at least 8 characters that contain uppercase, lowercase, alphanumeric, not the same word as username, can't contain words 'chicken', and 'ernie'\n"
	echo -n "Password: "
	read -s password
	lowerPassword=$(echo $password | tr '[:upper:]' '[:lower:]')
done

echo "$(date +"%D %T") REGISTER: INFO User $username registered successfully" >> log.txt

echo -e "\n"
echo "REGISTRATION SUCCESFUL"
echo "$username : $password" >> ./users/user.txt
