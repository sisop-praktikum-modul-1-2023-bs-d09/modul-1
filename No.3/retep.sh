#!/bin/bash
echo "LOGIN"

echo -n "Username: "
read username

takenUsername=$(grep -w "$username" ./users/user.txt)

while [ -z  "$takenUsername" ]
do
	echo "Username not identified, please try again"
	echo -n "Username: "
	read username
	takenUsername=$(grep -w "$username" ./users/user.txt)
done

existPassword=$(echo $takenUsername | awk '{print $3}')

echo -n "Password: "
read -s password

until [ $password == $existPassword ]
do
	echo "Password incorrect, try again"
	echo "$(date +"%D %T") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
	echo -n "Password: "
	read -s password
done

echo "$(date +"%D %T") LOGIN: INFO User $username logged in" >> log.txt

echo -e "\n"
echo "LOGIN SUCCESFUL"


