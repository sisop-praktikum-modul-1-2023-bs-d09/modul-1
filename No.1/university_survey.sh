#!/bin/bash

#No.1a
#Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
echo '5 Universitas dengan ranking tertinggi di Jepang diantaranya:'
sort -t, -k1n '2023 QS World University Rankings.csv' | awk -F',' '$3 == "JP" {print $2}' | head -n 5 

echo -e '\n'

#No.1b
#Cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas Jepang di poin 1a.
echo '5 Universitas dengan Faculty Student Score (FSR Score) paling rendah diantara Universitas di No.1a diantaranya:'
sort -t, -k1n '2023 QS World University Rankings.csv' | awk -F',' '$3 == "JP"' | head -n 5\
 | sort -t, -k9n | awk -F',' '{print $2}' | head -n 1

echo -e '\n'

#No.1c
#Cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
echo '10 Universitas di Jepang dengan Employment Outcome Rank (GER Rank) paling tinggi diantaranya:'
sort -t, -k20n '2023 QS World University Rankings.csv' | awk -F',' '$3 == "JP" {print $2}' | head -n 10

echo -e '\n'

#No.1d
#Mencari universitas tersebut dengan kata kunci keren.
echo 'Universitas yang memiliki kata kunci "keren" diantaranya:'
awk -F',' '/Keren/ {print $2}' '2023 QS World University Rankings.csv'
